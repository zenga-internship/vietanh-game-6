﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DemoController : MonoBehaviour
{
    [SerializeField] Transform m_Content;
    [SerializeField] StageItem m_StageItemPrefab;

    void Start()
    {
        for (int i = 0; i < 100; i++)
        {
            StageItem item = Instantiate(m_StageItemPrefab, m_Content);
            item.Setup(i + 1);
        }
    }
}
