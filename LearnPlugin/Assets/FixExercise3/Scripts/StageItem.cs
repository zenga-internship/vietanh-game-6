﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class StageItem : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI m_LevelText;
    [SerializeField] Image[] m_Stars;
    [SerializeField] Sprite m_StarOn;
    [SerializeField] Sprite m_StarOff;

    int m_Index;

    public void Setup(int index)
    {
        m_Index = index;
        m_LevelText.text = m_Index.ToString();

        int stars = Random.Range(0, 3);
        for (int i = 0; i < m_Stars.Length; i++)
        {
            m_Stars[i].sprite = (i <= stars) ? m_StarOn : m_StarOff;
        }
    }

    public void OnStageButtonTap()
    {
        Debug.Log(m_Index);
    }
}
