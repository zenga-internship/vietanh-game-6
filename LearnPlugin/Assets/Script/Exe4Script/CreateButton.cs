﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateButton : MonoBehaviour
{
    [SerializeField]
    private GameObject prefabs;
    [SerializeField]
    private Transform grid;
    //Khởi tạo chức năng controller
    [SerializeField]
    private PreFabsController controller;
    [SerializeField]
    public int listIndex;
    public void Start()
    {
        //Khởi tạo vòng lặp for
        for (int i = 0; i <= listIndex; i++)
        {
            //Gán số i = số num 
            controller.Num = i;
            //Gọi hàm changeNum 
            controller.changeNum();
            //Tạo Prefabs mới 
            Instantiate(prefabs, new Vector3(), Quaternion.identity, grid);

        }
    }

}