﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class PreFabsController : MonoBehaviour
{
    [SerializeField]
    TextMeshProUGUI numTxt;
    TextMeshProUGUI btnTxt;
    //Khai báo chuỗi ký tự
    public string[] txt;
    //Khai báo số index trung gian
    public int Num { set { num = value; } }
    int num;
    //Khai báo số  random trung gian 
    int ranNum;

    public void changeNum()
    {

        //gán ký tự số bằng só index
        numTxt.text = num.ToString();
        //Khởi tạo số random
        int randomNum = Random.Range(0, 3);
        //Gán số ngẫu nhiên = số trung gian
        ranNum = randomNum;
        //Gán ký tự trong text bằng với text theo giá trị randomNum trong mảng txt
        btnTxt.text = txt[randomNum];
    }

    public void showindex()
    {
        if (num % 3 == 0)
        {
            Debug.Log(ranNum + " " + btnTxt.text);
        }
        else
        {
            Debug.Log(num);
        }
    }
}


