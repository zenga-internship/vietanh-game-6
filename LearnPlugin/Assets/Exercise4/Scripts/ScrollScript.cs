﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mopsicus.InfiniteScroll;
using UnityEngine.UI;
using DG.Tweening;
public class ScrollScript : MonoBehaviour
{
    [SerializeField]
    private InfiniteScroll Scroll;

    [SerializeField]
    private int Count = 100;
    IEnumerator Start()
    {
        Scroll.OnFill += OnFillItem;
        Scroll.InitData(Demos.data.Count);
        for (int i = 0; i < Scroll.Views.Length; i++)
        {
            if (Scroll.Views[i].activeInHierarchy)
            {
                yield return new WaitForSeconds(.2f);
                Scroll.Views[i].transform.localScale = Vector3.one;
                Scroll.Views[i].transform.DOPunchScale(Vector3.one, 0.1f);
                if (AudioManager.instance != null)
                {
                    AudioManager.instance.PlayButtonTapSfx();
                }
            }
        }
    }

    void OnFillItem(int index, GameObject item)
    {
        item.transform.localScale = Vector3.zero;
        item.GetComponent<TopMenuItem>().Setup(Demos.data[index]);
    }

    // Update is called once per frame
}
