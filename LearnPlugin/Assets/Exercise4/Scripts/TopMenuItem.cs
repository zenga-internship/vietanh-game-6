﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class TopMenuItem : MonoBehaviour
{
    [SerializeField] string[] m_RandomTexts;
    [SerializeField] TextMeshProUGUI m_IndexText;
    [SerializeField] TextMeshProUGUI m_DisplayText;
    [SerializeField] Image image;
    [SerializeField] Sprite[] sprites;


    DemoData m_demodata;
    int m_Random;

    public void Setup(DemoData data)
    {
        m_demodata = data;
        m_IndexText.text = data.id.ToString();
        m_DisplayText.text = data.text;
        image.sprite = sprites[data.color%3];
        //transform.DOScale(1f,5f);
    }

    public void OnMenuItemTap()
    {

    }
}
