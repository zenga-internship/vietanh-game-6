﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LitJson;
public class DemoData
{
    public int id;
    public string text;
    public int color;
}

public class Demos
{
    static DemoData[] rawData;
    public static Dictionary<int, DemoData> data = new Dictionary<int, DemoData>();

    static Demos()
    {
        rawData = JsonMapper.ToObject<DemoData[]>(Resources.Load<TextAsset>("Datas/DemoData.json").text);
        for (int i = 0; i < rawData.Length; i++)
        {
            data.Add(rawData[i].id, rawData[i]);
        }
    }
}
